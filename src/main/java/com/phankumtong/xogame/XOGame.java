/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phankumtong.xogame;

import java.util.Scanner;

/**
 *
 * @author ROG
 */
public class XOGame {

    static int countTurn = 0;

    static Scanner kb = new Scanner(System.in);
    static boolean isFinish = false;
    static int row, column;
    static char player = 'X';
    static char winner;

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};

    static void showWelcome() {
        System.out.println("Wel come to OX Game");

    }

    static void showTable() {

        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(" " + (row + 1));
            for (int column = 0; column < table[row].length; column++) {
                System.out.print(" " + table[row][column]);

            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");

    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            column = kb.nextInt() - 1;

            if (table[row][column] == '-') {
                table[row][column] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }

    }

    static void checkWin() {
        if (table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O'
                || table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O'
                || table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O'
                || table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O'
                || table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O'
                || table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O'
                || table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
                || table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {

            winner = 'O';
            isFinish = true;

        } else if (table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X'
                || table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X'
                || table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X'
                || table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X'
                || table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X'
                || table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X'
                || table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
                || table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {

            winner = 'X';
            isFinish = true;

        } else if (countTurn == 9) {
            winner = 'D';
            isFinish = true;

        }

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }

    }

    static void showResult() {
        if (winner == 'X' || winner == 'O') {

            System.out.println("Player " + winner + " Win");
        } else {
            System.out.println("Draw");
        }

    }

    static void showBye() {
        System.out.println("Bye bye . . . .");

    }

    public static void main(String[] args) {
        do {
            countTurn++;
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();

    }
}
